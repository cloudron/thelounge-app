// https://github.com/thelounge/thelounge/blob/master/defaults/config.js

module.exports = {
    public: false,

    host: undefined, // all interfaces
    port: 9000,
    reverseProxy: true,
    maxHistory: 10000,

    https: {
        enable: false,
        key: "",
        certificate: "",
        ca: "",
    },

    theme: "default", // https://thelounge.github.io/docs/plugins/themes.html
    prefetch: true,
	disableMediaPreview: false,

    prefetchStorage: true, // proxies images via the server
    prefetchMaxImageSize: 2048,
	prefetchMaxSearchSize: 50,
	prefetchTimeout: 5000,

    fileUpload: {
        enable: true,
        maxFileSize: 10240 * 1024
    },

    transports: ["polling", "websocket"],

    leaveMessage: "The Lounge - https://thelounge.github.io",

    // default values for the connect form
	defaults: {
		name: "Libera.Chat",
		host: "irc.libera.chat",
		port: 6697,
		password: "",
		tls: true,
		rejectUnauthorized: true,
		nick: "thelounge%%",
		username: "thelounge",
		realname: "",
		join: "#thelounge",
		leaveMessage: "",
	},

    lockNetwork: false, // if users can edit configured networks
	messageStorage: ["sqlite", "text"],
    useHexIp: false,
    webirc: null,

    // this is not supported on Cloudron
    identd: {
        enable: false,
        port: 113
    },

    oidentd: null,

    ldap: {
        enable: !!process.env.CLOUDRON_LDAP_SERVER,
        url: process.env.CLOUDRON_LDAP_URL,
        // baseDN: process.env.CLOUDRON_LDAP_USERS_BASE_DN, // we use the searchDN method
        primaryKey: "username",

        searchDN: {
            rootDN: process.env.CLOUDRON_LDAP_BIND_DN,
            rootPassword: process.env.CLOUDRON_LDAP_BIND_PASSWORD,
            filter: "(objectclass=user)",
            base: process.env.CLOUDRON_LDAP_USERS_BASE_DN,
            scope: "sub",
        }
    },

    debug: {
        ircFramework: false,
        raw: false,
    },

};
