<nosso>

This app is pre-setup with an account. The initial credentials are:

**Username**: admin<br/>
**Password**: changeme<br/>

Please change the password immediately. See the [docs](https://cloudron.io/documentation/apps/thelounge) on how to add more users.

</nosso>
