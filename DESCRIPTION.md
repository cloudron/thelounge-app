The Lounge is a web IRC client that you host on your own server.

### Features

* Multiple user support
* Stays connected even when you close the browser
* Connect from multiple devices at once
* Responsive layout — works well on your smartphone
* .. and more!

