FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

USER cloudron
WORKDIR /home/cloudron

# renovate: datasource=github-releases depName=thelounge/thelounge versioning=semver extractVersion=^v(?<version>.+)$
ARG LOUNGE_VERSION=4.4.3

RUN curl -L https://github.com/thelounge/lounge/archive/v${LOUNGE_VERSION}.tar.gz | tar -xz --strip-components 1 -f -
    # npm i @babel/preset-env@7.9.0 && \
RUN npm install --legacy-peer-deps && \
    NODE_ENV=production npm run build && \
    npm cache clean --force

COPY start.sh /home/cloudron/
COPY lounge-config.js /home/cloudron/config.js
COPY lounge-demo-config.js /home/cloudron/demo-config.js

# set this here so that the lounge CLI can be run easily from web terminal
ENV THELOUNGE_HOME /app/data

# For user to install themes via npm
USER root
# npm config set cache --global /run/npmcache
RUN ln -s /run/npmcache /root/.npm && \
    ln -s /home/cloudron/index.js /usr/bin/thelounge

CMD [ "/home/cloudron/start.sh" ]

