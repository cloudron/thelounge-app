#!/bin/bash

set -eu

# THELOUNGE_HOME env var is set in Dockerfile.
# ~/storage - images and thumbnails
# ~/upload - uploaded files
# ~/packages - themes and plugins via `thelounge install`
# ~/users - user database
# ~/logs - irc logs! and not app logs
mkdir -p /run/npmcache /app/data/{storage,uploads}

rm -f /app/data/logs || true # this used to be a symlink in the past

if [[ ! -f /app/data/config.js ]]; then
    if [[ "${CLOUDRON_APP_DOMAIN}" == *demo.cloudron* ]]; then
        echo "Copying demo config on first run"
        cp /home/cloudron/demo-config.js /app/data/config.js
    else
        echo "Copying config on first run"
        cp /home/cloudron/config.js /app/data/config.js
    fi

    if [[ -z "${CLOUDRON_LDAP_SERVER:-}" ]]; then
        mkdir -p /app/data/users
        if [[ -z "$(ls -A /app/data/users)" ]]; then
            echo "Creating default admin user"
            echo -e "changeme\n\n" | node index.js add admin
        fi
    fi
else
    # cannot use json merge since the config is javascript
    sed -e "s/process.env.LDAP_/process.env.CLOUDRON_LDAP_/g" \
        -i /app/data/config.js
fi

chown -R cloudron:cloudron /app/data

echo "Starting lounge"
export NODE_ENV=production
exec /usr/local/bin/gosu cloudron:cloudron node index.js start

