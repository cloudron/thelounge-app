#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        return await browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(() => browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT));
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function login(username, password) {
        await browser.manage().deleteAllCookies();

        await browser.get('about:blank');
        await browser.sleep(4000);
        await browser.get('https://' + app.fqdn);
        await browser.sleep(3000);
        await browser.wait(until.elementIsVisible(browser.findElement(By.id('signin-username'))), TEST_TIMEOUT);
        await browser.findElement(By.id('signin-username')).sendKeys(Key.chord(Key.CONTROL, 'a'));
        await browser.findElement(By.id('signin-username')).sendKeys(username);
        await browser.findElement(By.id('signin-password')).sendKeys(password);
        await browser.findElement(By.xpath('//button[contains(text(), "Sign in")]')).click();
        await browser.wait(until.elementLocated(By.xpath('//h2[contains(text(), "User preferences")]')), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(By.xpath('//h2[contains(text(), "User preferences")]'))), TEST_TIMEOUT);
    }

    async function logout() {
        await browser.get('about:blank');
        await browser.sleep(4000);
        await browser.get(`https://${app.fqdn}/#/settings/account`);
        await waitForElement(By.xpath('//button[contains(., "Sign out")]'));
        await browser.findElement(By.xpath('//button[contains(., "Sign out")]')).click();
        await waitForElement(By.id('signin-username'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, username, password));
    it('can logout', logout);
    it('can restart app', function () { execSync('cloudron restart'); });
    it('can login', login.bind(null, username, password));
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login.bind(null, username, password));
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.manage().deleteAllCookies();
        await browser.get('about:blank');

        execSync('cloudron configure --location ' + LOCATION + '2', EXEC_ARGS);
        getAppInfo();
    });

    it('can login', login.bind(null, username, password));
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // No SSO
    it('install app (no sso)', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login (no sso)', login.bind(null, 'admin', 'changeme'));

    it('uninstall app (no sso)', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync('cloudron install --appstore-id io.github.thelounge --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can login', login.bind(null, username, password));
    it('can logout', logout);
    it('can update', function () { execSync('cloudron update --app ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, username, password));
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
