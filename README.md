# Lounge Cloudron App

This repository contains the Cloudron app package source for [Lounge](https://thelounge.github.io).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=io.github.thelounge)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id io.github.thelounge
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd thelounge-app
cloudron build
cloudron install
```

